<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="todolist.css">
    <title>To Do List</title>
</head>
<body>
    <h1 class="titre">To Do List</h1>
    <form action="" method="post">
        <textarea name="tache" id=""></textarea>
        <button type="submit" name="valider" class="valider">Valider</button>
    </form>

</body>
</html>

<?php
// démarrage de la session
session_start();

//isset permet de vérifié si la variable existe 
if (!isset($_SESSION['tache'])) {

    $_SESSION['tache'] = array();
}

// Pour voir si le formulaire a bien été saisi
if (isset($_POST['valider'])) {

    array_push($_SESSION['tache'], $_POST['valider']);
}

// J'uttilise une boucle pour afficher les taches (qui ne fonctionne pas bien entendu!!!!!)
foreach ($_SESSION['tache'] as $tache) {
    echo $tache . "";
}

// rien ne fonctionne. 
?>
